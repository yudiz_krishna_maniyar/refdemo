
import { useRef } from 'react';
import './App.css';
import Header from './Components/Header';
import List from './Components/List';

function App() {
  const ref =useRef()
  return (
    <div className="App">
      <Header re={ref}/>
      <List ref={ref} />
  
    </div>
  );
}

export default App;
