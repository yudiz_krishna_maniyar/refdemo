import React from 'react'

const  Header = (props) => {
  return (
    <div>
        
        <button
          className="k-button k-button-md k-rounded-md k-button-solid k-button-solid-primary"
          onClick={() => props.re.current.excelExport()}
        >
          Export to Excel
        </button>
     
    </div>
  );
}

export default Header