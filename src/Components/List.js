import React, {forwardRef, useImperativeHandle, useRef, useState} from "react";
import useFetch from '../Hook/useFetch'
import {  Grid, GridColumn } from "@progress/kendo-react-grid";
import { ExcelExport } from "@progress/kendo-react-excel-export";
import Users from "./Users.json";

const  List = forwardRef((props,ref) => {
  
  // use of customhook
  const [data] = useFetch("http://localhost:4000/users");

  const _export = useRef(null);
 
   useImperativeHandle(ref, () => ({
     excelExport,
   }));

  const excelExport = () => {
    if (_export.current !== null) {
      _export.current.save();
    }
  };

  return (
    <>
      <ExcelExport data={Users} ref={_export}>
        <Grid data={Users}>
          <GridColumn
            className="table"
            field="UsersID"
            title=" ID"
            width="50px"
          />

          <GridColumn
            className="table"
            field="UsersName"
            title=" Name"
            width="350px"
          />

          <GridColumn
            className="table"
            field="UsersEmail"
            title=" Name"
            width="350px"
          />
        </Grid>
      </ExcelExport>
       <h1>CustomHook</h1>
        {/* use of customhook */}
      {data &&
        data.map((item) => {
          return <p key={item.UsersID}>{item.UsersName}</p>
        })}
    </>
  );
});
 export default List